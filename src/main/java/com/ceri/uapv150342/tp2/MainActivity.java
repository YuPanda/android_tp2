package com.ceri.uapv150342.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import static com.ceri.uapv150342.tp2.BookDbHelper.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book book = new Book("","","","","");
                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                intent.putExtra("book",book);
                startActivity(intent);
            }
        });
        final ListView listView = (ListView) findViewById(R.id.listView);

        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                getMenuInflater().inflate(R.menu.menu_suppr,menu);
            }
        });

        final BookDbHelper bookHelper = new BookDbHelper(this);
        Cursor result = bookHelper.fetchAllBooks();
        if(result.getCount() == 0) {
            bookHelper.populate();
        }
        bookHelper.populate();


        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,result,new String[] {COLUMN_BOOK_TITLE,COLUMN_AUTHORS},
                new int[] {android.R.id.text1,android.R.id.text2},0);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor curseCurrent = (Cursor)parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book",cursorToBook(curseCurrent));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        final ListView listView = (ListView) findViewById(R.id.listView);
        final BookDbHelper bookHelper = new BookDbHelper(this);
        Cursor result = bookHelper.fetchAllBooks();
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,result,new String[] {COLUMN_BOOK_TITLE,COLUMN_AUTHORS},
                new int[] {android.R.id.text1,android.R.id.text2},0);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapter = (AdapterView) findViewById(R.id.listView);
        Cursor cursor = (Cursor) adapter.getItemAtPosition(info.position);
        BookDbHelper bookDbHelper = new BookDbHelper(MainActivity.this);
        bookDbHelper.deleteBook(cursor);
        Toast.makeText(MainActivity.this,"Supprimer",Toast.LENGTH_LONG).show();
        onResume();
        return true;
    }
}
