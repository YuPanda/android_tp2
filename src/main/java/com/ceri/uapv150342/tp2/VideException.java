package com.ceri.uapv150342.tp2;

import android.content.Context;
import android.widget.Toast;

public class VideException extends Exception
{
    public VideException(Context context, String mError) {
        Toast.makeText(context, mError, Toast.LENGTH_LONG).show();
    }
}
