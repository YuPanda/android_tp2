package com.ceri.uapv150342.tp2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Boolean modif = true;
        final EditText editTitre = findViewById(R.id.nameBook);
        final EditText editAuteur = findViewById(R.id.editAuthors);
        final EditText editAnnee = findViewById(R.id.editYear);
        final EditText editGenre = findViewById(R.id.editGenres);
        final EditText editEditeur = findViewById(R.id.editPublisher);
        final Book book = (Book) getIntent().getParcelableExtra("book");
        if(book.getTitle().length() == 0)
            modif = false;
        final Button save = findViewById(R.id.button);

        editTitre.setText(book.getTitle());
        editAuteur.setText(book.getAuthors());
        editAnnee.setText(book.getYear());
        editGenre.setText(book.getGenres());
        editEditeur.setText(book.getPublisher());

        final Boolean finalModif = modif;
        save.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                try{
                    if(editTitre.getText().length() == 0 || editAuteur.getText().length() == 0 || editAnnee.getText().length() == 0 || editGenre.getText().length() == 0 || editEditeur.getText().length() == 0)
                    {
                        throw new VideException(BookActivity.this,"Valeur vide");
                    }
                    book.setTitle(String.valueOf(editTitre.getText()));
                    book.setAuthors(String.valueOf(editAuteur.getText()));
                    try {
                        Integer.parseInt(String.valueOf(editAnnee.getText()));
                        book.setYear(String.valueOf(editAnnee.getText()));
                        book.setGenres(String.valueOf(editGenre.getText()));
                        book.setPublisher(String.valueOf(editEditeur.getText()));
                        BookDbHelper bookDbHelper = new BookDbHelper(BookActivity.this);
                        if(finalModif) {
                            bookDbHelper.updateBook(book);
                            Toast.makeText(BookActivity.this, "Sauvegarder", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else {
                            if (!bookDbHelper.addBook(book))
                                Toast.makeText(BookActivity.this,"Un livre portant le même nom existe déjà dans la base de données",Toast.LENGTH_LONG).show();
                            else {
                                Toast.makeText(BookActivity.this, "Sauvegarder", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    }
                    catch (NumberFormatException  e)
                    {
                        Toast.makeText(BookActivity.this,"Erreur, entier non saisie",Toast.LENGTH_LONG).show();
                    }
                }
                catch(VideException E)
                {

                }
            }
        });
    }
}
